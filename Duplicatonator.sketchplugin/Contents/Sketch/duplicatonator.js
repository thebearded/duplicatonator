var onRun = function(context) {
  var document = require('sketch/dom').getSelectedDocument();
  //var selection = context.selection[0];
  var selection = document.selectedLayers
  selection.forEach((layer) => {
    layer.duplicate();
    //layer.moveBackward();

      var Rectangle = require('sketch/dom').Rectangle
      var newFrame = new Rectangle(layer.frame);
      newFrame.x = layer.frame.x;
      newFrame.y = layer.frame.y+layer.frame.height+10;
      layer.frame = newFrame;
  });
}
